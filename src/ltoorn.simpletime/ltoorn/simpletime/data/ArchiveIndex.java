/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data;

import ltoorn.simpletime.data.xml.XmlElement;
import ltoorn.simpletime.data.xml.XmlTag;
import ltoorn.simpletime.data.xml.XmlUtil;
import ltoorn.simpletime.data.xml.XmlWritable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class ArchiveIndex implements XmlWritable {

    private final Map<String, Integer> identifierToBucketMap = new HashMap<>();

    public void add(String identifier, int bucket) {
        Objects.requireNonNull(identifier);
        if (identifierToBucketMap.containsKey(identifier)) {
            throw new IllegalArgumentException(String.format("ArchiveIndex -- Duplicate identifier: '%s'", identifier));
        }
        identifierToBucketMap.put(identifier, bucket);
    }

    public void addFromXmlElement(XmlElement xmlElement) {
        XmlUtil.assertExpectedXmlTag(xmlElement.getXmlTag(), XmlTag.INDEX_ENTRY);

        String identifier = xmlElement.getChildContentForTag(XmlTag.IDENTIFIER);
        int bucket = Integer.parseInt(xmlElement.getChildContentForTag(XmlTag.BUCKET));

        add(identifier, bucket);
    }

    public void remove(String identifier) {
        identifierToBucketMap.remove(identifier);
    }

    public boolean contains(String identifier) {
        return identifierToBucketMap.containsKey(identifier);
    }

    public Integer getBucket(String identifier) {
        return identifierToBucketMap.get(identifier);
    }

    public Set<String> getIdentifiers() {
        return Set.copyOf(identifierToBucketMap.keySet());
    }

    @Override
    public XmlElement toXmlElement() {
        XmlElement.Builder builder = XmlElement.builder(XmlTag.ARCHIVE_INDEX);

        identifierToBucketMap.forEach((identifier, bucket) -> builder.addChild(indexEntryToXmlElement(identifier, bucket)));

        return builder.build();
    }

    private XmlElement indexEntryToXmlElement(String identifier, Integer bucket) {
        return XmlElement.builder(XmlTag.INDEX_ENTRY)
                    .addChild(XmlElement.simple(XmlTag.IDENTIFIER, identifier))
                    .addChild(XmlElement.simple(XmlTag.BUCKET, bucket.toString()))
                    .build();
    }
}

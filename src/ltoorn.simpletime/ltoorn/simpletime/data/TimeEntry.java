/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data;

import ltoorn.simpletime.data.xml.XmlElement;
import ltoorn.simpletime.data.xml.XmlTag;
import ltoorn.simpletime.data.xml.XmlUtil;
import ltoorn.simpletime.data.xml.XmlWritable;
import ltoorn.simpletime.util.Util;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Objects;

public class TimeEntry implements XmlWritable {

   private final int timeEntryId;
   private final LocalDateTime start;
   private LocalDateTime end;

   private TimeEntry(int timeEntryId, LocalDateTime start, LocalDateTime end) {
      this.timeEntryId = timeEntryId;
      this.start = start;
      this.end = end;
   }

   private static TimeEntry newTimeEntry(int timeEntryId, LocalDateTime start, LocalDateTime end) {
      return new TimeEntry(timeEntryId, Objects.requireNonNull(start), end);
   }

   public static TimeEntry start(int timeEntryId) {
      return newTimeEntry(timeEntryId, LocalDateTime.now(), null);
   }

   public static TimeEntry fromXmlElement(XmlElement xmlElement) {
      XmlUtil.assertExpectedXmlTag(xmlElement.getXmlTag(), XmlTag.TIME_ENTRY);

      int timeEntryId = -1;
      LocalDateTime start = null;
      LocalDateTime end = null;

      for (XmlElement child : xmlElement.getChildren()) {
         switch (child.getXmlTag()) {
            case TIME_ENTRY_ID -> timeEntryId = Integer.parseInt(child.getContent());
            case START -> start = LocalDateTime.parse(child.getContent(), Util.canonicalTimeStampFormat);
            case END -> end = LocalDateTime.parse(child.getContent(), Util.canonicalTimeStampFormat);
            default -> throw new IllegalStateException(String.format("Unexpected XML element: '%s'", child.getXmlTag()));
         }
      }

      return newTimeEntry(timeEntryId, start, end);
   }

   public void close() {
      if (end != null) {
         throw new IllegalStateException("Time entry has already ended");
      }
      end = LocalDateTime.now();
   }

   public boolean betweenDates(LocalDate min, LocalDate max) {
      final boolean withinUpperBound = max == null || start.toLocalDate().isBefore(max);
      if (isOpen()) {
         return withinUpperBound;
      }
      final boolean withinLowerBound = min == null || end.toLocalDate().isAfter(min);
      return withinLowerBound && withinUpperBound;
   }

   public Duration toDuration() {
      if (end == null) {
         return Duration.ZERO;
      }
      return Duration.between(start, end);
   }

   public boolean isOpen() {
      return end == null;
   }

   public TimeEntry update(LocalDateTime start, LocalDateTime end) {
      return new TimeEntry(timeEntryId, Objects.requireNonNull(start), end);
   }

   @Override
   public XmlElement toXmlElement() {
      XmlElement.Builder builder = XmlElement.builder(XmlTag.TIME_ENTRY)
                  .addChild(XmlElement.simple(XmlTag.TIME_ENTRY_ID, Integer.toString(timeEntryId)))
                  .addChild(XmlElement.simple(XmlTag.START, start.format(Util.canonicalTimeStampFormat)));

      if (end != null) {
         builder.addChild(XmlElement.simple(XmlTag.END, end.format(Util.canonicalTimeStampFormat)));
      }

      return builder.build();
   }

   public String prettyPrint() {
      return "timeEntryId: " + timeEntryId + ", start: " + start.format(Util.canonicalTimeStampFormat) + ", end: " + (end != null ? end.format(Util.canonicalTimeStampFormat) : "");
   }

   @Override
   public boolean equals(Object other) {
      if (this == other) {
         return true;
      }
      if (other instanceof TimeEntry) {
         var otherTimeEntry = (TimeEntry) other;
         return timeEntryId == otherTimeEntry.timeEntryId && start.equals(otherTimeEntry.getStart()) && end.equals(otherTimeEntry.getEnd());
      }
      return false;
   }

   @Override
   public int hashCode() {
      int result = 1;
      result = 31 * result + timeEntryId;
      result = 31 * result + start.hashCode();
      result = 31 * result + end.hashCode();
      return result;
   }

   public int getTimeEntryId() {
      return timeEntryId;
   }

   public LocalDateTime getStart() {
      return start;
   }

   public LocalDateTime getEnd() {
      return end;
   }
}

/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data;

import ltoorn.simpletime.cmd.InvalidCommandException;
import ltoorn.simpletime.data.xml.XmlElement;
import ltoorn.simpletime.data.xml.XmlTag;
import ltoorn.simpletime.data.xml.XmlUtil;
import ltoorn.simpletime.data.xml.XmlWritable;
import ltoorn.simpletime.util.Util;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

public class Entry implements XmlWritable {

    private final String identifier;
    private final String description;
    private final LocalDate dateAdded;
    private LocalDateTime lastUpdated;

    private final List<TimeEntry> timeEntries;
    private final Set<EntryTag> entryTags;

    private final int identifierHashCode;
    private Duration totalTimeSpent;

    private Entry(Builder builder) {
        identifier = builder.identifier;
        description = builder.description;
        dateAdded = builder.dateAdded;
        lastUpdated = builder.lastUpdated;
        timeEntries = builder.timeEntries;
        entryTags = builder.entryTags;

        identifierHashCode = builder.identifier.hashCode();
        totalTimeSpent = timeEntries.stream().map(TimeEntry::toDuration).reduce(Duration.ZERO, Duration::plus);
    }

    public static Entry makeNewEntry(String identifier, String description) {
        return new Builder().setIdentifier(identifier).setDescription(description).build();
    }

    public static Entry fromXmlElement(XmlElement xmlElement) {
        XmlUtil.assertExpectedXmlTag(xmlElement.getXmlTag(), XmlTag.ENTRY);

        Builder builder = new Builder();
        for (XmlElement child : xmlElement.getChildren()) {
            switch (child.getXmlTag()) {
                case IDENTIFIER -> builder.setIdentifier(child.getContent());
                case DESCRIPTION -> builder.setDescription(child.getContent());
                case DATE_ADDED -> builder.setDateAdded(LocalDate.parse(child.getContent(), Util.canonicalDateFormat));
                case LAST_UPDATED -> builder.setLastUpdated(LocalDateTime.parse(child.getContent(), Util.canonicalTimeStampFormat));
                case TIME_ENTRIES -> builder.setTimeEntries(child.getChildren().stream().map(TimeEntry::fromXmlElement).collect(Collectors.toList()));
                case ENTRY_TAGS -> builder.setEntryTags(child.getChildren().stream().map(EntryTag::fromXmlElement).collect(Collectors.toSet()));
                default -> throw new IllegalStateException(String.format("Unknown Entry field in Entry XML: '%s'", child.getXmlTag().tagName));
            }
        }

        return builder.build();
    }

    public static String validateIdentifier(String identifier) throws InvalidEntryException {
        Objects.requireNonNull(identifier);
        if (identifier.isEmpty()) {
            throw new InvalidEntryException("Empty entry identifiers are not allowed.");
        }
        IntPredicate isLegalCharacter = c -> Character.isLetterOrDigit(c) || c == '-' || c == '_';
        for (int codePoint : identifier.codePoints().toArray()) {
            if (!isLegalCharacter.test(codePoint)) {
                String msg = String.format("Illegal character used in entry identifier '%s'. Offending character '%c'.", identifier, codePoint);
                throw new InvalidEntryException(msg);
            }
        }
        return identifier;
    }

    public boolean isOpen() {
        return timeEntries.stream().anyMatch(TimeEntry::isOpen);
    }

    public void start() {
        int timeEntryId = timeEntries.size();
        timeEntries.add(TimeEntry.start(timeEntryId));
        lastUpdated = LocalDateTime.now();
    }

    public void end() {
        TimeEntry openEntry = timeEntries.stream()
                    .filter(t -> t.getEnd() == null)
                    .findFirst()
                    .orElseThrow(() -> new InvalidCommandException(String.format("Entry '%s' was already closed.", identifier)));

        openEntry.close();

        totalTimeSpent = totalTimeSpent.plus(openEntry.toDuration());
        lastUpdated = LocalDateTime.now();
    }

    public Entry update(Integer timeEntryId, LocalDateTime start, LocalDateTime end, String newDescription) {
        if (newDescription == null) {
            newDescription = description;
        }

        Function<TimeEntry, TimeEntry> timeEntryMapping = te -> {
            if (Objects.equals(timeEntryId, te.getTimeEntryId()) && start != null) {
                return te.update(start, end);
            }
            return te;
        };

        return cloneBuilder()
                    .setTimeEntries(timeEntries.stream().map(timeEntryMapping).collect(Collectors.toList()))
                    .setDescription(newDescription)
                    .build();
    }

    public Entry updateTags(Set<EntryTag> tags, boolean invert) {
        Set<EntryTag> updatedEntryTags = new HashSet<>(entryTags);
        if (invert) {
            updatedEntryTags.removeAll(tags);
        } else {
            updatedEntryTags.addAll(tags);
        }
        return cloneBuilder().setEntryTags(updatedEntryTags).build();
    }

    public boolean anyMatchForEntryTags(Set<EntryTag> entryTagsToTest) {
        return entryTags.stream().anyMatch(entryTagsToTest::contains);
    }

    @Override
    public XmlElement toXmlElement() {
        return XmlElement.builder(XmlTag.ENTRY)
                    .addChild(XmlElement.simple(XmlTag.IDENTIFIER, identifier))
                    .addChild(XmlElement.simple(XmlTag.DESCRIPTION, description))
                    .addChild(XmlElement.simple(XmlTag.DATE_ADDED, dateAdded.format(Util.canonicalDateFormat)))
                    .addChild(XmlElement.simple(XmlTag.LAST_UPDATED, lastUpdated.format(Util.canonicalTimeStampFormat)))
                    .addChild(XmlUtil.buildXml(timeEntries, XmlTag.TIME_ENTRIES))
                    .addChild(XmlUtil.buildXml(entryTags, XmlTag.ENTRY_TAGS))
                    .build();
    }

    public boolean matches(int identifierHashCode, String identifier) {
        return this.identifierHashCode == identifierHashCode && this.identifier.equals(identifier);
    }

    public String prettyPrint() {
        StringBuilder builder = new StringBuilder();

        builder.append("Entry ").append(identifier).append(" {")
                    .append("\n\tdescription: ").append(description)
                    .append("\n\tdate added: ").append(dateAdded.format(Util.canonicalDateFormat))
                    .append("\n\tlast updated: ").append(lastUpdated.format(Util.canonicalTimeStampFormat))
                    .append("\n\ttotal time spent: ").append(Util.printDurationHoursMinutesSeconds(totalTimeSpent))
                    .append("\n\ttime entries {");
        timeEntries.forEach(te -> builder.append("\n\t\t").append(te.prettyPrint()));
        builder.append("\n\t}");
        builder.append("\n\ttags {");
        entryTags.forEach(tag -> builder.append("\n\t\t").append(tag.prettyPrint()));
        builder.append("\n\t}\n}");

        return builder.toString();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "@[ identifier: " + identifier + " ]";
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof Entry) {
            Entry otherEntry = (Entry) other;
            if (otherEntry.hashCode() == this.hashCode()) {
                return otherEntry.getIdentifier().equals(this.identifier);
            }
        }
        return false;
    }

    public boolean anyTimeEntryBetween(LocalDate min, LocalDate max) {
        return timeEntries.stream().anyMatch(te -> te.betweenDates(min, max));
    }

    @Override
    public int hashCode() {
        return identifierHashCode;
    }

    public String getIdentifier() {
        return identifier;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public List<TimeEntry> getTimeEntries() {
        return List.copyOf(timeEntries);
    }

    private Builder cloneBuilder() {
        return new Builder()
                    .setIdentifier(identifier)
                    .setDescription(description)
                    .setDateAdded(dateAdded)
                    .setTimeEntries(timeEntries)
                    .setEntryTags(entryTags);
    }

    private static class Builder {

        private String identifier;
        private String description = "";
        private LocalDate dateAdded = LocalDate.now();
        private LocalDateTime lastUpdated = LocalDateTime.now();

        private final List<TimeEntry> timeEntries = new ArrayList<>();
        private final Set<EntryTag> entryTags = new HashSet<>();

        private Entry build() {
            if (identifier == null) {
                throw new InvalidEntryException("Cannot build entry without identifier.");
            }
            return new Entry(this);
        }

        private Builder setIdentifier(String identifier) {
            this.identifier = validateIdentifier(identifier);
            return this;
        }

        private Builder setDescription(String description) {
            if (description != null) {
                this.description = description;
            }
            return this;
        }

        private Builder setDateAdded(LocalDate dateAdded) {
            this.dateAdded = Objects.requireNonNull(dateAdded);
            return this;
        }

        private Builder setLastUpdated(LocalDateTime lastUpdated) {
            this.lastUpdated = Objects.requireNonNull(lastUpdated);
            return this;
        }

        private Builder setTimeEntries(List<TimeEntry> timeEntries) {
            this.timeEntries.clear();
            this.timeEntries.addAll(timeEntries);
            return this;
        }

        private Builder setEntryTags(Set<EntryTag> entryTags) {
            this.entryTags.clear();
            this.entryTags.addAll(entryTags);
            return this;
        }
    }
}

package ltoorn.simpletime.data;

import java.io.Serial;

public class InvalidEntryException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 757875847747L;
    public InvalidEntryException(String message) {
        super(message);
    }
}

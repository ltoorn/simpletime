/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class XmlElement {

    private final XmlTag xmlTag;
    private final String content;
    private final List<XmlElement> children;

    private XmlElement(Builder builder) {
        this.xmlTag = builder.xmlTag;
        this.content = builder.content;
        this.children = builder.children;
    }

    public static Builder builder(XmlTag tag) {
        return new Builder(tag);
    }

    public static XmlElement simple(XmlTag tag, String content) {
        return new Builder(tag).addContent(content).build();
    }

    public String print() {
        return printWithDepth(0);
    }

    private String printWithDepth(final int depth) {

        StringBuilder result = new StringBuilder();

        final String indentation = "\t".repeat(depth);

        result.append(indentation).append("<").append(xmlTag).append(">").append(XmlUtil.escapeForXml(content));

        if (!children.isEmpty()) {
            result.append("\n");
            for (XmlElement child : children) {
                result.append(child.printWithDepth(depth + 1));
            }
            result.append(indentation);
        }

        result.append("</").append(xmlTag).append(">\n");

        return result.toString();
    }

    public XmlTag getXmlTag() {
        return xmlTag;
    }

    public String getContent() {
        return content;
    }

    public List<XmlElement> getChildren() {
        return children;
    }

    public String getChildContentForTag(XmlTag tag) {
        return children.stream()
                    .filter(child -> child.getXmlTag() == tag)
                    .findAny()
                    .map(XmlElement::getContent)
                    .orElseThrow();
    }

    public static class Builder {

        private final XmlTag xmlTag;
        private String content = "";
        private final List<XmlElement> children = new ArrayList<>();

        private Builder(XmlTag xmlTag) {
            this.xmlTag = xmlTag;
        }

        public Builder addContent(String content) {
            this.content = Objects.requireNonNull(content);
            return this;
        }

        public Builder addChild(XmlElement child) {
            children.add(child);
            return this;
        }

        public XmlElement build() {
            return new XmlElement(this);
        }
    }
}

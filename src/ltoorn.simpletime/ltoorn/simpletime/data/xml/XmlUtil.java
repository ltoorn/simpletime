/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.xml;

import java.util.Collection;
import java.util.stream.Collectors;

public class XmlUtil {

    public static final int LT = '<';
    public static final int GT = '>';
    public static final int AMP = '&';
    public static final int APOS = '\'';
    public static final int QUOT = '"';

    private XmlUtil() {}

    public static String escapeForXml(String raw) {
        return raw.codePoints().mapToObj(XmlUtil::escapeCodePointForXml).collect(Collectors.joining());
    }

    private static String escapeCodePointForXml(int codePoint) {
        return switch (codePoint) {
            case LT -> "&lt;";
            case GT -> "&gt;";
            case AMP -> "&amp;";
            case APOS -> "&apos;";
            case QUOT -> "&quot;";
            default -> Character.toString(codePoint);
        };
    }

    public static XmlElement buildXml(Collection<? extends XmlWritable> writableElements, XmlTag rootTag) {
        XmlElement.Builder builder = XmlElement.builder(rootTag);
        for (XmlWritable element : writableElements) {
            builder.addChild(element.toXmlElement());
        }
        return builder.build();
    }

    public static void assertExpectedXmlTag(XmlTag actual, XmlTag expected) {
        if (actual != expected) {
            throw new IllegalStateException(String.format("Expected: '%s', got: '%s'", expected.tagName, actual.tagName));
        }
    }
}

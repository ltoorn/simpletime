package ltoorn.simpletime.data.xml;

import java.io.Serial;

public class InvalidXmlException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 743733849L;
    public InvalidXmlException(String message) {
        super(message);
    }
}

/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.xml;

public enum XmlTag {

    SIMPLETIME("simpletime"),

    ENTRIES("entries"),
    ENTRY("entry"),
    IDENTIFIER("identifier"),
    DESCRIPTION("description"),
    DATE_ADDED("dateAdded"),
    LAST_UPDATED("lastUpdated"),
    TIME_ENTRIES("timeEntries"),

    TIME_ENTRY("timeEntry"),
    TIME_ENTRY_ID("timeEntryId"),
    START("start"),
    END("end"),

    ARCHIVE_INDEX("archiveIndex"),
    INDEX_ENTRY("indexEntry"),
    BUCKET("bucket"),

    ENTRY_TAGS("entryTags"),
    ENTRY_TAG("entryTag");

    public final String tagName;

    XmlTag(String tagName) {
        this.tagName = tagName;
    }

    public static XmlTag fromString(String tagName) {
        for (XmlTag tag : XmlTag.values()) {
            if (tag.tagName.equals(tagName)) {
                return tag;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return tagName;
    }
}

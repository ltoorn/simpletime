/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.persistence;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.xml.XmlElement;
import ltoorn.simpletime.data.xml.XmlTag;
import ltoorn.simpletime.data.xml.XmlUtil;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * Class for writing simpletime XML files.
 */
public class XmlWriter {

    public static final String STANDARD_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

    private XmlWriter() {}

    /**
     * Writes the provided list of entries to the current entries XML file.
     */
    public static void writeToCurrentEntries(List<Entry> entries) throws IOException {
        entries.sort(Comparator.comparing(Entry::getLastUpdated).reversed());
        writeEntries(FileSystem.CURRENT_FILE_ABS_PATH, entries);
    }

    /**
     * Writes the provided list of entries to the XML archive files, creating new buckets as necessary.
     */
    public static void writeToArchive(Collection<Entry> entries) throws IOException, XMLStreamException {

        ArchiveIndex index = XmlReader.readArchiveIndex();

        int highestBucketNumber = ArchiveUtil.MIN_BUCKET_NUMBER;
        for (File bucket : ArchiveUtil.getBucketFiles()) {
            int bucketNumber = ArchiveUtil.extractBucketNumber(bucket);
            if (bucketNumber > highestBucketNumber) {
                highestBucketNumber = bucketNumber;
            }
        }
        List<Entry> bucketEntries = new ArrayList<>(XmlReader.readEntriesFromPath(ArchiveUtil.createBucketPath(highestBucketNumber)));
        int remainingCapacity = ArchiveUtil.determineRemainingCapacityForEntries(bucketEntries.size());

        for (Entry entry : entries) {
            if (remainingCapacity == 0) {
                writeEntries(ArchiveUtil.createBucketPath(highestBucketNumber), bucketEntries);
                bucketEntries.clear();
                remainingCapacity = ArchiveUtil.MAX_BUCKET_CAPACITY;
                highestBucketNumber++;
            }
            bucketEntries.add(entry);
            index.add(entry.getIdentifier(), highestBucketNumber);
            remainingCapacity--;
        }

        if (!bucketEntries.isEmpty()) {
            writeEntries(ArchiveUtil.createBucketPath(highestBucketNumber), bucketEntries);
        }

        writeArchiveIndex(index);
    }

    public static void writeArchiveIndex(ArchiveIndex index) throws IOException {
        writeXml(FileSystem.ARCHIVE_INDEX_ABS_PATH, index.toXmlElement());
    }

    public static void writeEntries(Path targetFile, List<Entry> entries) throws IOException {
        writeXml(targetFile, XmlUtil.buildXml(entries, XmlTag.ENTRIES));
    }

    public static void writeEntryTags(Set<EntryTag> entryTags) throws IOException {
        writeXml(FileSystem.TAGS_ABS_PATH, XmlUtil.buildXml(entryTags, XmlTag.ENTRY_TAGS));
    }

    private static void writeXml(Path path, XmlElement xml) throws IOException {
        Files.writeString(path, STANDARD_HEADER + xml.print());
    }
}

/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.persistence;

import java.io.File;
import java.nio.file.Path;

public class FileSystem {

    public static final String USER_HOME = System.getProperty("user.home");
    public static final String ROOT_DIR_NAME = "simpletime";
    public static final String CURRENT_FILE_NAME = "current.xml";
    public static final String ARCHIVE_DIR_NAME = "archive";
    public static final String ARCHIVE_BUCKETS_DIR_NAME = "buckets";
    public static final String ARCHIVE_INDEX_FILE_NAME = "index.xml";
    public static final String ARCHIVE_FILE_NAME_PREFIX = "bucket_";
    public static final String TAGS_FILE_NAME = "tags.xml";

    public static final Path ROOT_DIR_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME).toAbsolutePath();
    public static final Path CURRENT_FILE_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME, CURRENT_FILE_NAME).toAbsolutePath();
    public static final Path ARCHIVE_DIR_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME, ARCHIVE_DIR_NAME).toAbsolutePath();
    public static final Path ARCHIVE_BUCKETS_DIR_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME, ARCHIVE_DIR_NAME, ARCHIVE_BUCKETS_DIR_NAME).toAbsolutePath();
    public static final Path ARCHIVE_INDEX_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME, ARCHIVE_DIR_NAME, ARCHIVE_INDEX_FILE_NAME).toAbsolutePath();
    public static final Path TAGS_ABS_PATH = Path.of(USER_HOME, ROOT_DIR_NAME, TAGS_FILE_NAME).toAbsolutePath();

    private FileSystem() {}

    /**
     * Initializes the directories used internally by the simpletime database.
     */
    public static boolean init() {

        File root = ROOT_DIR_ABS_PATH.toFile();
        if (!root.exists()) {
            if (!root.mkdir()) {
                return false;
            }
        }

        File archiveRoot = ARCHIVE_DIR_ABS_PATH.toFile();
        if (!archiveRoot.exists()) {
            if (!archiveRoot.mkdir()) {
                return false;
            }
        }

        File archiveBucketsDir = ARCHIVE_BUCKETS_DIR_ABS_PATH.toFile();
        if (!archiveBucketsDir.exists()) {
            if (!archiveBucketsDir.mkdir()) {
                return false;
            }
        }

        return true;
    }
}

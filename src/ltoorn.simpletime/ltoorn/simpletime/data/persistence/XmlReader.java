/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.persistence;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.xml.InvalidXmlException;
import ltoorn.simpletime.data.xml.XmlElement;
import ltoorn.simpletime.data.xml.XmlTag;
import ltoorn.simpletime.data.xml.XmlUtil;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Class to read simpletime XML files.
 */
public class XmlReader {

    private XmlReader() {}

    /**
     * Reads the current entries from file.
     * @return A list of entries extracted from the current entries XML file.
     */
    public static List<Entry> readCurrentEntries() throws IOException, XMLStreamException {
        return readEntriesFromPath(FileSystem.CURRENT_FILE_ABS_PATH);
    }

    /**
     * Reads the entries from the XML file at the provided path.
     * @return A list of entries extracted from the read XML file.
     */
    public static List<Entry> readEntriesFromPath(Path src) throws IOException, XMLStreamException {
        XmlElement entries = read(src);
        if (entries == null) {
            return new ArrayList<>();
        }
        XmlUtil.assertExpectedXmlTag(entries.getXmlTag(), XmlTag.ENTRIES);
        return entries.getChildren().stream().map(Entry::fromXmlElement).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Reads the {@link ArchiveIndex} from file.
     * @return The archive index read from the archive index XMl file.
     */
    public static ArchiveIndex readArchiveIndex() throws IOException, XMLStreamException {
        ArchiveIndex archiveIndex = new ArchiveIndex();
        XmlElement indices = read(FileSystem.ARCHIVE_INDEX_ABS_PATH);
        if (indices == null) {
            return archiveIndex;
        }
        indices.getChildren().forEach(archiveIndex::addFromXmlElement);
        return archiveIndex;
    }

    /**
     * Reads the entry tags from file.
     * @return The set of entry tags extracted from the entry tags XML file.
     */
    public static Set<EntryTag> readEntryTags() throws XMLStreamException, IOException {
        XmlElement tags = read(FileSystem.TAGS_ABS_PATH);
        if (tags == null) {
            return Collections.emptySet();
        }
        XmlUtil.assertExpectedXmlTag(tags.getXmlTag(), XmlTag.ENTRY_TAGS);
        return tags.getChildren().stream().map(t -> EntryTag.fromString(t.getContent())).collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * Reads a simpletime XML file and transforms it into an internal representation.
     * @param src The path of the simpletime XML file to read.
     * @return The root {@link XmlElement}.
     */
    private static XmlElement read(Path src) throws IOException, XMLStreamException {

        final File srcFile = src.toFile();
        if (!srcFile.exists() || Files.size(src) == 0) {
            return null;
        }
        XmlElement result = null;
        // We keep a stack of XML element builders, to track scoping (i.e. the first element to get pushed onto the stack will be the root
        // element, and for each subsequent element, the top element will be a child of the element below it.)
        Stack<XmlElement.Builder> builderStack = new Stack<>();
        XMLInputFactory inputFactory = XMLInputFactory.newDefaultFactory();
        try (InputStream fileInputStream = new FileInputStream(srcFile)) {
            XMLEventReader eventReader = inputFactory.createXMLEventReader(fileInputStream, StandardCharsets.UTF_8.name());
            while (eventReader.hasNext()) {
                final XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String name = event.asStartElement().getName().toString();
                    XmlTag tag = XmlTag.fromString(name);
                    if (tag == null) {
                        throw new InvalidXmlException(String.format("Invalid XML: '%s'; Encountered an unknown tag: '%s'", src.getFileName().toString(), name));
                    }
                    builderStack.push(XmlElement.builder(tag));
                    // We peek ahead to see if this tag has character content we need to add to it.
                    XMLEvent peekedEvent = eventReader.peek();
                    if (peekedEvent != null && peekedEvent.isCharacters()) {
                        if (!peekedEvent.asCharacters().getData().isBlank()) {
                            builderStack.peek().addContent(eventReader.getElementText().trim());
                            XmlElement element = builderStack.pop().build();
                            builderStack.peek().addChild(element);
                        }
                    }
                } else if (event.isEndElement()) {
                    XmlElement element = builderStack.pop().build();
                    if (!builderStack.isEmpty()) {
                        builderStack.peek().addChild(element);
                    } else {
                        result = element;
                        break;
                    }
                }
            }
        }
        return Objects.requireNonNull(result);
    }
}

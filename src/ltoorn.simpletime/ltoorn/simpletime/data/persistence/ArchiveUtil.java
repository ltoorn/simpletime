/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.data.persistence;

import ltoorn.simpletime.util.Util;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;

public class ArchiveUtil {

    public static final int MAX_BUCKET_CAPACITY = 100;
    public static final int MIN_BUCKET_NUMBER = 0;

    private ArchiveUtil() {}

    public static int determineRemainingCapacityForEntries(int currentNumberOfEntries) {
        return (int) Util.clampToZero(MAX_BUCKET_CAPACITY - currentNumberOfEntries);
    }

    public static File[] getBucketFiles() {
        File bucketsDir = FileSystem.ARCHIVE_BUCKETS_DIR_ABS_PATH.toFile();
        return Objects.requireNonNull(bucketsDir.listFiles(), "Unable to read archive");
    }

    public static Path createBucketPath(int bucketNumber) {
        String nextBucketFileName = FileSystem.ARCHIVE_FILE_NAME_PREFIX + bucketNumber + ".xml";
        return FileSystem.ARCHIVE_BUCKETS_DIR_ABS_PATH.resolve(nextBucketFileName).toAbsolutePath();
    }

    public static int extractBucketNumber(File bucketFile) {
        final int prefixLength = FileSystem.ARCHIVE_FILE_NAME_PREFIX.length();
        return Integer.parseInt(bucketFile.getName().substring(prefixLength, prefixLength+1));
    }
}

/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime;

import ltoorn.simpletime.cmd.Command;
import ltoorn.simpletime.cmd.InvalidCommandException;
import ltoorn.simpletime.data.InvalidEntryException;
import ltoorn.simpletime.data.persistence.FileSystem;
import ltoorn.simpletime.data.xml.InvalidXmlException;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException, XMLStreamException {
        if (!FileSystem.init()) {
            fatal("Failed to initialize simpletime files.");
        }
        try {
            Command cmd = Command.parse(args);
            cmd.process();
            String output = cmd.getOutput();
            if (output != null && !output.isBlank()) {
                System.out.println(output);
            }
        } catch (InvalidCommandException | InvalidEntryException | InvalidXmlException e) {
            fatal(e.getMessage());
        }
    }

    public static void fatal(String msg) {
        System.err.println(msg);
        System.exit(1);
    }
}

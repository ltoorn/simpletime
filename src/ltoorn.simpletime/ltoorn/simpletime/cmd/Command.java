/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.data.persistence.XmlWriter;
import ltoorn.simpletime.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class to parse and process all commands.
 */
public abstract class Command {

    private Command() {}

    /**
     * Parses and validates the provided command line args.
     * @return An appropriate Command object based on the contents of the command line args, that can be used to execute the
     * command and retrieve any output if applicable.
     */
    public static Command parse(String[] commandLineArgs) {
        Objects.requireNonNull(commandLineArgs);

        final int len = commandLineArgs.length;
        if (len == 0) {
            throw new InvalidCommandException("Simpletime: A minimal time tracking program.\n\nFor usage enter the `help` command.");
        }

        class Arg {
            int i = 0;
            String next() {
                if (i < len) {
                    return commandLineArgs[i++];
                }
                return null;
            }
        }
        final Arg arg = new Arg();

        final String cmd = Objects.requireNonNull(arg.next());
        final int nrOfArgs = len - 1;

        switch (cmd) {
            case "help" -> {
                validateNumberOfArguments(nrOfArgs, 0, 1);
                return new HelpCommand(arg.next());
            }
            case "add" -> {
                validateNumberOfArguments(nrOfArgs, 1, 2);
                return new AddCommand(arg.next(), arg.next());
            }
            case "startNew" -> {
                validateNumberOfArguments(nrOfArgs, 1, 2);
                return new StartNewCommand(arg.next(), arg.next());
            }
            case "start" -> {
                validateNumberOfArguments(nrOfArgs, 1, 1);
                return new StartCommand(arg.next());
            }
            case "end" -> {
                validateNumberOfArguments(nrOfArgs, 0, 1);
                return new EndCommand(arg.next());
            }
            case "delete" -> {
                validateNumberOfArguments(nrOfArgs, 1, 1);
                return new DeleteCommand(arg.next());
            }
            case "createTag" -> {
                validateNumberOfArguments(nrOfArgs, 0, 1);
                return new CreateTagCommand(arg.next());
            }
            case "archive", "current", "update", "retrieve", "tag", "report" -> {
                // All validation for the query arguments is done by the query builder.
                Query.Builder queryBuilder = Query.builder(cmd);
                for (int i = 1; i < commandLineArgs.length; i++) {
                    queryBuilder.addArgValuePair(commandLineArgs[i]);
                }
                return new QueryCommand(queryBuilder.build());
            }
            default -> throw new InvalidCommandException(String.format("Unknown command: '%s'", cmd));
        }
    }

    private static void validateNumberOfArguments(int actual, int min, int max) {
        if (actual < min || actual > max) {
            String msg = String.format("Illegal number of arguments: %d, expected at least %d and at most %d.", actual, min, max);
            throw new InvalidCommandException(msg);
        }
    }

    /**
     * Adds an entry to the provided list, after checking against duplicates, both in the provided list and in the currently valid {@link ArchiveIndex}.
     */
    private static void addEntryToList(Entry entry, List<Entry> entries) throws IOException, XMLStreamException {
        if (entries.contains(entry)) {
            throw new InvalidCommandException(String.format("Trying to add duplicate entry: %s", entry.getIdentifier()));
        }
        ArchiveIndex index = XmlReader.readArchiveIndex();
        if (index.contains(entry.getIdentifier())) {
            throw new InvalidCommandException(String.format("An entry with the identifier '%s' already exists in the archive", entry.getIdentifier()));
        }
        entries.add(entry);
    }

    private static List<Entry> addToCurrentEntries(Entry entry) throws IOException, XMLStreamException {
        List<Entry> entries = XmlReader.readCurrentEntries();
        Command.addEntryToList(entry, entries);
        return entries;
    }

    private static Entry find(String identifier, List<Entry> entries) {
        final int identifierHashCode = identifier.hashCode();
        Optional<Entry> entry = entries.stream().filter(e -> e.matches(identifierHashCode, identifier)).findFirst();
        if (entry.isEmpty()) {
            throw new InvalidCommandException(String.format("Entry '%s' not found", identifier));
        }
        return entry.get();
    }

    /**
     * Processes the command. Commands typically terminate silently when successful. Any output to be communicated to the user
     * should be made available by means of {@link #getOutput}. Any error messages should be passed up the call chain as
     * the message field of an appropriate subclass of RuntimeException.
     */
    public abstract void process() throws IOException, XMLStreamException;

    /**
     * Returns any output resulting from the execution of this command. Most commands don't return any output on successful execution.
     */
    public String getOutput() {
        return null;
    }

    private static class HelpCommand extends Command {

        private final String queryName;

        private String output;

        private HelpCommand(String queryName) {
            this.queryName = queryName;
        }

        @Override
        public void process() {
            output = """
                        simpletime, a minimal time tracking program. Usage:
                        \t`help` <optional queryName> -- Prints this help message, or the help message for the provided queryName. Available query commands: `archive`, `current`, `update`, `retrieve`, `tag`, `report`.
                        \t`add` <identifier> <optional description> -- Adds a new entry to the list of current entries.
                        \t`startNew` <identifier> <optional description> -- Adds a new entry to the list of current entries, and starts a time entry for it.
                        \t`start` <identifier> -- Starts a time entry for the entry in the current entries list identified by the provided identifier.
                        \t`end` <optional identifier> -- Ends the time entry for the entry in the current entries list identified by the provided identifier, or ends any open time entry when no identifier was provided.
                        \t`delete` <identifier> -- Deletes the entry identified by the provided identifier from the list of current entries.
                        \t`createTag` <optional tagName> -- Adds a tag with the provided tagName to the list of tags, or prints all tags in the list of tags when no tagName was provided.""";

            if (queryName != null) {
                Query.QueryType queryType = Query.QueryType.fromQueryCommand(queryName);
                if (queryType == null) {
                    String msg = String.format(
                                "Unknown query name: '%s'. Valid query names are: %s"
                                , queryName
                                , Arrays.stream(Query.QueryType.values()).map(q -> String.format("'%s'", q.toString())).collect(Collectors.joining(", "))
                    );
                    throw new InvalidCommandException(msg);
                }
                output = Query.buildQueryUsageMessage(queryType);
            }
        }

        @Override
        public String getOutput() {
            return output;
        }
    }

    private static class AddCommand extends Command {

        private final String identifier;
        private final String description;

        private AddCommand(String identifier, String description) {
            this.identifier = identifier;
            this.description = description;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            Entry entry = Entry.makeNewEntry(identifier, description);
            List<Entry> entries = Command.addToCurrentEntries(entry);
            XmlWriter.writeToCurrentEntries(entries);
        }
    }

    private static class StartNewCommand extends Command {

        private final String identifier;
        private final String description;

        private StartNewCommand(String identifier, String description) {
            this.identifier = identifier;
            this.description = description;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            List<Entry> entries = XmlReader.readCurrentEntries();
            Util.closeAllOpenEntries(entries);
            Entry entry = Entry.makeNewEntry(identifier, description);
            entry.start();
            Command.addEntryToList(entry, entries);
            XmlWriter.writeToCurrentEntries(entries);
        }
    }

    private static class StartCommand extends Command {

        private final String identifier;

        private StartCommand(String identifier) {
            this.identifier = identifier;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            List<Entry> entries = XmlReader.readCurrentEntries();
            Util.closeAllOpenEntries(entries);
            Command.find(identifier, entries).start();
            XmlWriter.writeToCurrentEntries(entries);
        }
    }

    private static class EndCommand extends Command {

        private final String identifier;

        private EndCommand(String identifier) {
            this.identifier = identifier;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            List<Entry> entries = XmlReader.readCurrentEntries();
            if (identifier != null) {
                Command.find(identifier, entries).end();
            } else {
                Util.closeAllOpenEntries(entries);
            }
            XmlWriter.writeToCurrentEntries(entries);
        }
    }

    private static class DeleteCommand extends Command {

        private final String identifier;

        private DeleteCommand(String identifier) {
            this.identifier = identifier;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            List<Entry> entries = XmlReader.readCurrentEntries();
            Entry entryToDelete = Command.find(identifier, entries);
            List<Entry> updatedEntries = entries.stream().filter(e -> !e.equals(entryToDelete)).collect(Collectors.toList());
            XmlWriter.writeToCurrentEntries(updatedEntries);
        }
    }

    private static class CreateTagCommand extends Command {

        private final String tag;

        private String output;

        private CreateTagCommand(String tag) {
            this.tag = tag;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            Set<EntryTag> entryTags = new HashSet<>(XmlReader.readEntryTags());
            if (tag == null) {
                output = entryTags.stream().map(EntryTag::toString).sorted().collect(Collectors.joining("\n"));
                return;
            }
            entryTags.add(EntryTag.fromString(tag));
            XmlWriter.writeEntryTags(entryTags);
        }

        @Override
        public String getOutput() {
            return output;
        }
    }

    private static class QueryCommand extends Command {

        private final Query query;

        private QueryCommand(Query query) {
            this.query = query;
        }

        @Override
        public void process() throws IOException, XMLStreamException {
            query.execute();
        }

        @Override
        public String getOutput() {
            return query.output();
        }
    }
}
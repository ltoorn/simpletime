/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.persistence.ArchiveUtil;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.data.persistence.XmlWriter;
import ltoorn.simpletime.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Retrieve implements Query {

    private final Set<String> identifiers;
    private final Set<EntryTag> entryTags;
    private final LocalDate minDate;
    private final LocalDate maxDate;
    private final boolean reactivate;

    private String output;

    public Retrieve(Builder builder)  {
        identifiers = builder.identifiers;
        entryTags = builder.entryTags;
        minDate = builder.minDate;
        maxDate = builder.maxDate;
        reactivate = builder.reactivate;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void execute() throws IOException, XMLStreamException {
        final ArchiveIndex index = XmlReader.readArchiveIndex();

        if (identifiers.isEmpty() && entryTags.isEmpty() && minDate == null && maxDate == null) {
            output = index.getIdentifiers().stream().sorted().collect(Collectors.joining("\n"));
            return;
        }

        // If the user hasn't passed any identifiers to filter on, we filter on all identifiers present in the index. This
        // enables the user to filter only on entryTag or date.
        if (identifiers.isEmpty()) {
            identifiers.addAll(index.getIdentifiers());
        }

        final Map<Integer, Set<String>> bucketToIdentifiers = Util.mapBucketsToIdentifiers(index, identifiers);
        final Map<Integer, List<Entry>> bucketToEntries = new HashMap<>();
        for (Integer bucket : bucketToIdentifiers.keySet()) {
            List<Entry> entriesInBucket = XmlReader.readEntriesFromPath(ArchiveUtil.createBucketPath(bucket));
            bucketToEntries.put(bucket, entriesInBucket);
        }

        final Predicate<Entry> predicate = Query.buildIdentifierPredicate(identifiers)
                    .and(Query.buildDatePredicate(minDate, maxDate))
                    .and(Query.buildEntryTagsPredicate(entryTags));

        Set<Entry> retrievedEntries = bucketToEntries.values().stream()
                    .flatMap(List::stream)
                    .filter(predicate)
                    .collect(Collectors.toSet());

        if (reactivate) {
            for (Map.Entry<Integer, List<Entry>> mapEntry : bucketToEntries.entrySet()) {
                List<Entry> updatedEntriesForBucket = mapEntry.getValue().stream().filter(e -> !retrievedEntries.contains(e)).collect(Collectors.toList());
                XmlWriter.writeEntries(ArchiveUtil.createBucketPath(mapEntry.getKey()), updatedEntriesForBucket);
            }
            retrievedEntries.forEach(e -> index.remove(e.getIdentifier()));

            XmlWriter.writeArchiveIndex(index);

            List<Entry> currentEntries = XmlReader.readCurrentEntries();
            currentEntries.addAll(retrievedEntries);
            XmlWriter.writeToCurrentEntries(currentEntries);

            return;
        }

        output = retrievedEntries.stream()
                    .map(Entry::prettyPrint)
                    .collect(Collectors.joining("\n"));
    }

    @Override
    public String output() {
        return output;
    }

    public static class Builder implements Query.Builder {

        private final Set<String> identifiers = new HashSet<>();
        private final Set<EntryTag> entryTags = new HashSet<>();
        private LocalDate minDate;
        private LocalDate maxDate;
        private boolean reactivate = false;

        private final FilterValidator filterValidator = new FilterValidator(QueryType.RETRIEVE);

        @Override
        public void addArgValuePair(String filterArgumentPair) {
            final Query.FilterArgument pair = Query.FilterArgument.processInputString(filterArgumentPair, filterValidator);
            switch (pair.filter) {
                case IDENTIFIER -> identifiers.addAll(Query.parseCommaSeparatedValues(pair.argument, Entry::validateIdentifier));
                case TAG -> entryTags.addAll(Query.parseCommaSeparatedValues(pair.argument, EntryTag::fromString));
                case MIN_DATE -> minDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
                case MAX_DATE -> maxDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
                case REACTIVATE -> reactivate = Boolean.parseBoolean(pair.argument);
            }
        }

        @Override
        public Query build() {
            return new Retrieve(this);
        }
    }
}

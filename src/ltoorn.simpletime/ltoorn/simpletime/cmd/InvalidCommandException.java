package ltoorn.simpletime.cmd;

import java.io.Serial;

public class InvalidCommandException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 938293849L;
    public InvalidCommandException(String message) {
        super(message);
    }
}

/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.TimeEntry;
import ltoorn.simpletime.data.persistence.ArchiveUtil;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Report implements Query {

    private final LocalDate minDate;
    private final LocalDate maxDate;

    private String output;

    private Report(Builder builder) {
        minDate = builder.minDate;
        maxDate = builder.maxDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void execute() throws IOException, XMLStreamException {
        final List<Entry> entries = XmlReader.readCurrentEntries();

        for (File bucketFile : ArchiveUtil.getBucketFiles()) {
            List<Entry> entriesInBucket = XmlReader.readEntriesFromPath(bucketFile.toPath().toAbsolutePath());
            entries.addAll(entriesInBucket);
        }
        final List<Entry> entriesToReport = entries.stream().filter(Query.buildDatePredicate(minDate, maxDate)).collect(Collectors.toList());

        final Map<LocalDate, Map<String, Duration>> timeSpentPerDayPerEntry = new TreeMap<>();
        for (Entry entryToReport : entriesToReport) {
            final String identifierToReport = entryToReport.getIdentifier();
            for (TimeEntry timeEntry : entryToReport.getTimeEntries()) {
                if (timeEntry.isOpen()) {
                    continue;
                }
                final LocalDate dateOfStartTimeEntry = timeEntry.getStart().toLocalDate();
                timeSpentPerDayPerEntry.compute(
                            dateOfStartTimeEntry
                            , (identifier, timeSpentForIdentifier) -> {
                                if (timeSpentForIdentifier == null) {
                                    timeSpentForIdentifier = new TreeMap<>();
                                }
                                timeSpentForIdentifier.merge(identifierToReport, timeEntry.toDuration(), Duration::plus);
                                return timeSpentForIdentifier;
                            }
                );
            }
        }

        output = prettyPrint(timeSpentPerDayPerEntry);
    }

    private static String prettyPrint(Map<LocalDate, Map<String, Duration>> timeSpentPerDayPerEntry) {
        List<String> outputElements = new ArrayList<>();
        timeSpentPerDayPerEntry.forEach((date, timeSpentForIdentifier) -> {
            StringBuilder builder = new StringBuilder();
            builder.append(date.format(Util.canonicalDateFormat)).append(" {");
            timeSpentForIdentifier.forEach((identifier, timeSpent) ->
                        builder.append("\n\t")
                                    .append(identifier)
                                    .append(": ")
                                    .append(Util.printDurationHoursMinutesSeconds(timeSpent)));
            builder.append("\n}");
            outputElements.add(builder.toString());
        });
        return String.join("\n", outputElements);
    }

    @Override
    public String output() {
        return output;
    }

    private static class Builder implements Query.Builder {

        private LocalDate minDate;
        private LocalDate maxDate;

        private final FilterValidator filterValidator = new FilterValidator(QueryType.CURRENT);

        @Override
        public void addArgValuePair(String filterArgumentPair) {
            final Query.FilterArgument pair = Query.FilterArgument.processInputString(filterArgumentPair, filterValidator);
            switch (pair.filter) {
                case MIN_DATE -> minDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
                case MAX_DATE -> maxDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
            }
        }

        @Override
        public Query build() {
            return new Report(this);
        }
    }
}

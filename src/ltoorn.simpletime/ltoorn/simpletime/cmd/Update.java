/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.data.persistence.XmlWriter;
import ltoorn.simpletime.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Update implements Query {

    private final Set<String> identifiers;

    private final LocalDate minDate;
    private final LocalDate maxDate;

    private final String description;
    private final Integer timeEntryId;
    private final LocalDateTime start;
    private final LocalDateTime end;

    private Update(Builder builder) {
        identifiers = builder.identifiers;
        timeEntryId = builder.timeEntryId;
        minDate = builder.minDate;
        maxDate = builder.maxDate;
        description = builder.description;
        start = builder.start;
        end = builder.end;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void execute() throws IOException, XMLStreamException {
        final ArchiveIndex archiveIndex = XmlReader.readArchiveIndex();
        if (identifiers.stream().anyMatch(archiveIndex::contains)) {
            throw new InvalidCommandException(String.format("Cannot updated archived entries: '%s'", identifiers.stream().filter(archiveIndex::contains).collect(Collectors.joining(", "))));
        }

        final Predicate<Entry> predicate = Query.buildIdentifierPredicate(identifiers).and(Query.buildDatePredicate(minDate, maxDate));
        final Function<Entry, Entry> mapping = e -> {
            if (predicate.test(e)) {
                return e.update(timeEntryId, start, end, description);
            }
            return e;
        };

        final List<Entry> updatedEntries = XmlReader.readCurrentEntries()
                    .stream()
                    .map(mapping)
                    .collect(Collectors.toList());

        XmlWriter.writeToCurrentEntries(updatedEntries);
    }

    public static class Builder implements Query.Builder {

        private final Set<String> identifiers = new HashSet<>();

        private LocalDate minDate;
        private LocalDate maxDate;

        private String description;
        private Integer timeEntryId;
        private LocalDateTime start;
        private LocalDateTime end;

        private final FilterValidator filterValidator = new FilterValidator(QueryType.UPDATE);

        @Override
        public void addArgValuePair(String argValuePair) {
            final Query.FilterArgument pair = Query.FilterArgument.processInputString(argValuePair, filterValidator);
            switch (pair.filter) {
                case IDENTIFIER -> identifiers.addAll(Query.parseCommaSeparatedValues(pair.argument, Entry::validateIdentifier));
                case MIN_DATE -> minDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
                case MAX_DATE -> maxDate = LocalDate.parse(pair.argument, Util.canonicalDateFormat);
                case DESCRIPTION -> description = pair.argument;
                case TIME_ENTRY_ID -> timeEntryId = Integer.parseInt(pair.argument);
                case START -> start = LocalDateTime.parse(pair.argument, Util.userInputTimeStampFormat);
                case END -> end = LocalDateTime.parse(pair.argument, Util.userInputTimeStampFormat);
            }
        }

        @Override
        public Query build() {
            return new Update(this);
        }
    }
}

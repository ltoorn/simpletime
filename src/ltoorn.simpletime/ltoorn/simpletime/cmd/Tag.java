/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.data.persistence.XmlWriter;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Tag implements Query {

    private final Set<String> identifiers;
    private final Set<EntryTag> entryTags;
    private final boolean invert;

    private Tag(Builder builder) {
        identifiers = builder.identifiers;
        entryTags = builder.entryTags;
        invert = builder.invert;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public void execute() throws IOException, XMLStreamException {
        final ArchiveIndex archiveIndex = XmlReader.readArchiveIndex();
        final Set<EntryTag> knownEntryTags = XmlReader.readEntryTags();

        if (identifiers.stream().anyMatch(archiveIndex::contains)) {
            throw new InvalidCommandException(String.format("Cannot update archived entries: '%s'", identifiers.stream().filter(archiveIndex::contains).collect(Collectors.joining(", "))));
        }

        for (EntryTag tag : entryTags) {
            if (!knownEntryTags.contains(tag)) {
                throw new InvalidCommandException(String.format("Encountered unknown tag: '%s'", tag));
            }
        }

        final Predicate<Entry> predicate = Query.buildIdentifierPredicate(identifiers);
        final Function<Entry, Entry> mapping = e -> {
            if (predicate.test(e)) {
                return e.updateTags(entryTags, invert);
            }
            return e;
        };

        final List<Entry> updatedEntries = XmlReader.readCurrentEntries()
                    .stream()
                    .map(mapping)
                    .collect(Collectors.toList());

        XmlWriter.writeToCurrentEntries(updatedEntries);
    }

    public static class Builder implements Query.Builder {

        private final Set<String> identifiers = new HashSet<>();
        private final Set<EntryTag> entryTags = new HashSet<>();

        private boolean invert = false;

        private final FilterValidator filterValidator = new FilterValidator(QueryType.TAG);

        @Override
        public void addArgValuePair(String filterArgumentPair) {
            final Query.FilterArgument pair = Query.FilterArgument.processInputString(filterArgumentPair, filterValidator);
            switch (pair.filter) {
                case IDENTIFIER -> identifiers.addAll(Query.parseCommaSeparatedValues(pair.argument, Entry::validateIdentifier));
                case TAG -> entryTags.addAll(Query.parseCommaSeparatedValues(pair.argument, EntryTag::fromString));
                case INVERT -> invert = Boolean.parseBoolean(pair.argument);
            }
        }

        @Override
        public Query build() {
            return new Tag(this);
        }
    }
}

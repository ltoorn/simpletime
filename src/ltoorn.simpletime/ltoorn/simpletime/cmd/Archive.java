/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.cmd;

import ltoorn.simpletime.data.Entry;
import ltoorn.simpletime.data.EntryTag;
import ltoorn.simpletime.data.persistence.XmlReader;
import ltoorn.simpletime.data.persistence.XmlWriter;
import ltoorn.simpletime.util.Util;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Archive implements Query {

    private final Set<String> identifiers;
    private final Set<EntryTag> entryTags;
    private final boolean invert;
    private final LocalDate minDate;
    private final LocalDate maxDate;

    private Archive(Builder builder) {
        identifiers = builder.identifiers;
        entryTags = builder.entryTags;
        invert = builder.invert;
        minDate = builder.dateFilters.get(Filter.MIN_DATE);
        maxDate = builder.dateFilters.get(Filter.MAX_DATE);
    }

    public static Query.Builder builder() {
        return new Builder();
    }

    @Override
    public void execute() throws IOException, XMLStreamException {
        final List<Entry> entries = XmlReader.readCurrentEntries();

        Predicate<Entry> predicate = Query.buildIdentifierPredicate(identifiers)
                    .and(Query.buildDatePredicate(minDate, maxDate))
                    .and(Query.buildEntryTagsPredicate(entryTags));

        if (invert) {
            predicate = predicate.negate();
        }

        final Set<Entry> entriesToArchive = new HashSet<>();
        for (Entry entry : entries) {
            if (predicate.test(entry)) {
                if (entry.isOpen()) {
                    throw new InvalidCommandException(String.format("Cannot archive unclosed entry '%s'", entry.getIdentifier()));
                }
                entriesToArchive.add(entry);
            }
        }

        if (!entriesToArchive.isEmpty()) {
            XmlWriter.writeToArchive(entriesToArchive);

            final List<Entry> updatedCurrentEntries = entries.stream().filter(e -> !entriesToArchive.contains(e)).collect(Collectors.toList());
            XmlWriter.writeToCurrentEntries(updatedCurrentEntries);
        }
    }

    public static class Builder implements Query.Builder {

        private final Set<String> identifiers = new HashSet<>();
        private final Set<EntryTag> entryTags = new HashSet<>();
        private final Map<Filter, LocalDate> dateFilters = new HashMap<>();

        private boolean invert = false;

        private final Query.FilterValidator filterValidator = new Query.FilterValidator(QueryType.ARCHIVE);

        @Override
        public void addArgValuePair(String filterArgumentPair) {
            final FilterArgument pair = FilterArgument.processInputString(filterArgumentPair, filterValidator);
            switch (pair.filter) {
                case IDENTIFIER -> identifiers.addAll(Query.parseCommaSeparatedValues(pair.argument, Entry::validateIdentifier));
                case TAG -> entryTags.addAll(Query.parseCommaSeparatedValues(pair.argument, EntryTag::fromString));
                case INVERT -> invert = Boolean.parseBoolean(pair.argument);
                case MIN_DATE, MAX_DATE -> dateFilters.put(pair.filter, LocalDate.parse(pair.argument, Util.canonicalDateFormat));
            }
        }

        @Override
        public Query build() {
            return new Archive(this);
        }
    }
}

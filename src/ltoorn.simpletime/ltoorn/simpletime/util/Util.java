/*
 * MIT License
 *
 * Copyright © 2021 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.simpletime.util;

import ltoorn.simpletime.data.ArchiveIndex;
import ltoorn.simpletime.data.Entry;

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Util {

    public static final DateTimeFormatter canonicalDateFormat = DateTimeFormatter.ofPattern("uuuu-MM-dd");
    public static final DateTimeFormatter canonicalTimeStampFormat = DateTimeFormatter.ofPattern("uuuu-MM-dd:HH.mm.ss.nn");
    public static final DateTimeFormatter userInputTimeStampFormat = DateTimeFormatter.ofPattern("uuuu-MM-dd:HH.mm");

    private Util() {}

    /**
     * Returns the provided duration in a simple, human readable format. This function is meant to be used in (pretty printed) output,
     * no logic should ever depend on the particular format it uses.
     */
    public static String printDurationHoursMinutesSeconds(Duration duration) {
        final long hours = clampToZero(duration.toHours());

        final Duration remainderMinusHours = duration.minusHours(hours);
        final long minutes = clampToZero(remainderMinusHours.toMinutes());

        final Duration remainderMinusMinutes = remainderMinusHours.minusMinutes(minutes);
        final long seconds = clampToZero(remainderMinusMinutes.toSeconds());

        return hours + " hours, " + minutes + " minutes, " + seconds + " seconds";
    }

    public static void closeAllOpenEntries(List<Entry> entries) {
        entries.stream().filter(Entry::isOpen).forEach(Entry::end);
    }

    public static long clampToZero(long n) {
        return n < 0 ? 0 : n;
    }

    public static Map<Integer, Set<String>> mapBucketsToIdentifiers(ArchiveIndex index, Collection<String> identifiers) {
        final Map<Integer, Set<String>> bucketToIdentifiers = new HashMap<>();
        for (String identifier : identifiers) {
            if (index.contains(identifier)) {
                Integer bucket = index.getBucket(identifier);
                bucketToIdentifiers.computeIfAbsent(bucket, k -> new HashSet<>()).add(identifier);
            }
        }
        return bucketToIdentifiers;
    }
}